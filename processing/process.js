import * as readline from "readline";
import * as fs from "fs";

async function processLineByLine() {
  const fileStream = fs.createReadStream("./data.in");

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity,
  });

  const writePath = "../web/keyArray.js";
  fs.writeFileSync(writePath, "export const keyArray = [");
  // Each line in data.in will be successively available here as `line`.
  for await (const line of rl) {
    fs.appendFileSync(writePath, line + ",");
  }
  fs.appendFileSync(writePath, "];");
}

console.log("Working...");
processLineByLine();
