<h1 style="display: flex; align-items: center; justify-content: left;">
    <img src="./logo.png" alt="drawing" width="40"/>
    <span style="margin-left: 15px">
        Keyboard Key Graph
    </span>
</h1>

If you know how many time you press a key but don't have any idea on how to visualize your keyboard key presses? Here its a tool that will help you do that, based on some input data this app will display on a graph each key press.

Here are two different small apps, one that converts from file input to JavaScript array and another one that uses that array to show correctly the graph.

## Installation

First you need to have [Node.js](https://nodejs.org/en/) installed.

After that for ease of use you can install `live-server` using `npm`:

```bat
npm install -g live-server
```

## Usage

1\. In `/processing/data.in` you have to put your key codes collection, line by line.

2\. Start the preprocessorr from processing to generate the array:

```bash
cd processing
```

```bat
node process.js
```

3\. Start the web UI:

```bash
cd ../web
```

```bat
live-server
```

## Note

This graphs work based on key codes and the numpad 'Return' key seems to have the same keycode as default 'Return' key. Also probably many of you don't know what 'Return' key is, that's just because you know it by a diffrent name, 'Enter', those two are realated. More info about 'Return' / 'Enter' key [here](https://en.wikipedia.org/wiki/Enter_key).

## Screenshot

![App Screenshot](./example.png)
