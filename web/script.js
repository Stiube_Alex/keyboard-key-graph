import { keyArray } from "./keyArray.js";

const keyOccurrences = {};
keyArray.forEach((key) => (keyOccurrences[key] ? keyOccurrences[key]++ : (keyOccurrences[key] = 1)));

const keys = document.querySelectorAll("div[name]");

keys.forEach((keyElement) => {
  const htmlNameValue = keyElement.attributes.name.value;
  const keyPressOccurrencesValue = keyOccurrences[htmlNameValue];
  console.log(htmlNameValue + ": " + keyPressOccurrencesValue);

  if (keyPressOccurrencesValue >= 1000) {
    keyElement.classList.add("insaneUse");
    return;
  }

  if (keyPressOccurrencesValue >= 500) {
    keyElement.classList.add("highUse");
    return;
  }

  if (keyPressOccurrencesValue >= 250) {
    keyElement.classList.add("highMidUse");
    return;
  }

  if (keyPressOccurrencesValue >= 100) {
    keyElement.classList.add("midUse");
    return;
  }

  if (keyPressOccurrencesValue >= 50) {
    keyElement.classList.add("lowMidUse");
    return;
  }

  if (keyPressOccurrencesValue >= 10) {
    keyElement.classList.add("lowUse");

    return;
  }
});
